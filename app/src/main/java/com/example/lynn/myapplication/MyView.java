package com.example.lynn.myapplication;

import android.content.Context;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Created by lynn on 11/17/2017.
 */

public class MyView extends LinearLayout {

    public MyView(Context context) {
        super(context);

        Button button = new Button(context);

        addView(button);
    }

}
